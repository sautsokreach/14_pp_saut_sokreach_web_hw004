import React from 'react'

class Input extends React.Component {
  constructor(){
    super();
    this.state = {name:""}
      }
  HandleOnChange = (e) =>{
    this.setState({ name: e.target.value });
    this.props.FGetName(e.target.value);

  }
  
  render() {
   return   <input type={"text"} placeholder="Type here" className="input w-full max-w-xs bg-blue-200 " value={this.state.name} onChange={this.HandleOnChange}/>
  
  }
}
export default Input;
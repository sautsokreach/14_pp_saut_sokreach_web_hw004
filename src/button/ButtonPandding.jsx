import React from 'react'

class ButtonPending extends React.Component {
 
  onClick =() =>{
    this.props.Onclick(this.props.id)
  };
  render() {
   return   <button className={`w-2/5 text-white focus:ring-4  font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2  ${this.props.color}`} onClick={this.onClick}>{this.props.value}</button> 
  }
}
export default ButtonPending;

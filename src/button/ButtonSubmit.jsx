import React from 'react'

export default function ButtonSubmit(props) {
  return (
    <button className="btn btn-active mb-6 w-4/12" onClick={props.onClick}>Register</button> 
  )
}

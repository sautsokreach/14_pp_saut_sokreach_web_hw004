import React from 'react'
import ButtonSubmit from './ButtonSubmit';
import Input from './Input';
import InputAge from './InputAge';
import InputEmail from './InputEmail';
import InputUsername from './InputUsername';
import TableStudent from './TableStudent';
class FormInput extends React.Component {
    constructor(){
        super()
        this.state= {liststudent:[],name:"",email:"",age:""}
        }
    getName = (e) =>{
        this.setState({name:e});
    }
    getEmail = (e) =>{
        this.setState({email:e});
    }
    getAge = (e) =>{
        this.setState({age:e});
    }
    getId = (e) =>{
        const newState = []
        for(var i of this.state.liststudent){
            console.log(i.status)
            if(i.id==e){
                i.status=i.status=="Pending"?"Done":"Pending"
                newState.push(i)
            }else{
                newState.push(i)
            }
        }
        this.setState({liststudent:newState})
    }
    onClick =() =>{
        const obj ={id:this.state.liststudent.length+1,name:this.state.name,email:this.state.email,age:this.state.age,status:"Pending"}
        this.setState({liststudent:[...this.state.liststudent,obj]})
    };
    render() {
     return  <div className='bg-lime-100 min-h-screen'>
    <h1 className="mb-4 text-3xl font-extrabold text-gray-900 dark:text-white md:text-5xl lg:text-6xl"><span className="text-transparent bg-clip-text bg-gradient-to-r to-emerald-600 from-sky-400">Please Fill Your </span>Information</h1>
    <div className="w-5/6 mx-auto">
     <InputEmail FGetValue={this.getEmail} ></InputEmail>
     <InputUsername FGetValue={this.getName}></InputUsername>
     <InputAge FGetValue={this.getAge}></InputAge>
     <ButtonSubmit onClick={this.onClick}></ButtonSubmit>
     <TableStudent studentlist={this.state.liststudent} FGetValue={this.getId}></TableStudent>
     </div>
    </div>
    
    }
  }
  export default FormInput;


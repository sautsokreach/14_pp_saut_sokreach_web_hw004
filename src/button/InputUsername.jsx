import React from 'react'

class InputUsername extends React.Component {
  constructor(){
    super();
    this.state = {name:""}
      }
  HandleOnChange = (e) =>{
    this.setState({ name: e.target.value });
    this.props.FGetValue(e.target.value);

  }
  
  render() {
   return  <div>
    <label htmlFor="website-admin" className="block mb-2 text-base font-medium text-gray-900 dark:text-white text-left">Username</label>
<div className="flex mb-6">
  <span className="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
    @
  </span>
  <input type="text" id="website-admin" className="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="elonmusk" value={this.state.email} onChange={this.HandleOnChange} />
</div>
   </div>
  
  }
}
export default InputUsername;
import ButtonPending from './ButtonPandding'
import ButtonShowMore from './ButtonShowMore'
import React, { Component } from 'react'

export default class TableStudent extends Component {
    onClick  = (e) =>{
        this.props.FGetValue(e)
    }
  render() {
    return (
        <div className="overflow-x-auto">

        <div className="relative overflow-x-auto">
          <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
              <thead  key="thead" className=" text-xs text-gray-700 uppercase bg-transparent dark:bg-gray-700 dark:text-gray-400 text-center">
                  <tr>
                      <th scope="col" className="px-6 py-3">
                          ID
                      </th>
                      <th scope="col" className="px-6 py-3">
                          Email
                      </th>
                      <th scope="col" className="px-6 py-3">
                          UserName
                      </th>
                      <th scope="col" className="px-6 py-3">
                          Age
                      </th>
                      <th scope="col" className="px-6 py-3">
                          Action
                      </th>
                  </tr>
              </thead>
              <tbody  className="[&>*:nth-child(even)]:bg-pink-200 text-center">
              {this.props.studentlist.map((item) => (
                      <tr   key={item.id} className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                            <td className="px-6 py-4">{item.id}</td>
                            <td className="px-6 py-4">{item.email==''?'Null':item.email}</td>
                            <td className="px-6 py-4">{item.name==''?'Null':item.name}</td>
                            <td className="px-6 py-4">{item.age==''?'Null':item.age}</td>
                            <td className="px-6 py-4"><ButtonPending color={item.status!='Pending'?"bg-green-600":"bg-red-600"} value={item.status} id={item.id} Onclick={this.onClick} ></ButtonPending >
                            <ButtonShowMore id={item.id} email={item.email} name={item.name} age={item.age}></ButtonShowMore></td>
                      </tr>
                    ))}
              </tbody>
          </table>
      </div>
      </div>
    )
  }
}


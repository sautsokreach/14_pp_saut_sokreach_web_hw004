import React from 'react'
import 'animate.css';
const Swal = require('sweetalert2')

// CommonJS
class ButtonShowMore extends React.Component {
  constructor(){
      super()
      }
  onClick =() =>{
    Swal.fire({
      title: `id : ${this.props.id} \n email : ${this.props.email} \n name : ${this.props.name} \n age : ${this.props.age}`,
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    })
  };
  render() {
   return   <button className={`w-2/5 focus:outline-none text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2`} onClick={this.onClick}>Show More</button> 
  }
}
export default ButtonShowMore;